package com.example.networktest;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    Button btnSubmit;
    Button btnCalc;

    String srvAnswer;
    TextView srvAnswerView;

    String matrikelNr;
    EditText matrikelNrView;

    Socket clientSocket;

    private static boolean isPrime(int num) {
        if (num == 0) return false;

        int upperBound = (int) Math.sqrt(num) + 1;

        for (int i = 2; i < upperBound; i++) {
            if (num % i == 0) return false;
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        matrikelNrView = findViewById(R.id.matrikelNumber);
        srvAnswerView = findViewById(R.id.srvAnswer);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnCalc = findViewById(R.id.btnCalc);

        btnSubmit.setOnClickListener(v -> handleSubmitBtn());
        btnCalc.setOnClickListener(v -> handleCalcBtn());
    }

    private void handleSubmitBtn() {
        matrikelNr = matrikelNrView.getText().toString();
        MatrikelNumberNetworkCall networkCall = new MatrikelNumberNetworkCall();

        networkCall.start();
        try {
            networkCall.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        srvAnswerView.setText(srvAnswer);
    }

    private void handleCalcBtn() {
        matrikelNr = matrikelNrView.getText().toString();
        char[] chars = matrikelNr.toCharArray();
        int[] numbers = new int[matrikelNr.length()];
        StringBuilder calcOut = new StringBuilder();

        for (int i = 0; i < matrikelNr.length(); i++) {
            numbers[i] = Character.getNumericValue(chars[i]);
        }

        Arrays.sort(numbers);

        for (int i = 0; i < matrikelNr.length(); i++) {
            if (!isPrime(numbers[i])) {
                calcOut.append(numbers[i]);
            }
        }

        srvAnswerView.setText(calcOut.toString());
    }

    private class MatrikelNumberNetworkCall extends Thread {
        @Override
        public void run() {
            try {
                clientSocket = new Socket("se2-isys.aau.at", 53212);

                DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                outToServer.writeBytes(matrikelNr + '\n');

                srvAnswer = inFromServer.readLine();

                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
